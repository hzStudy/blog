# nodejs 中 path.resolve 和 path.join 的区别

## path.resolve : 相当与命令行中的 cd ，返回绝对路径

### 示例(windows 下执行)

```js
path.resolve("/a", "./b"); // 'C:\\a\\b' 相当于 cd /a/b
```

```js
path.resolve("/a", "/b"); // 'C:\\b' 相当于 cd /a cd /b
```

```js
path.resolve("/a", "./b", "./c", "../d"); // 'C:\\a\\b\\d' 相当于 cd /a/b/c 然后返回上级目录再 ./d
```

## path.join : 只是简单的拼合路径

### 示例(windows 下执行)

```js
path.join("/a", "./b"); // '\\a\\b'
```

```js
path.join("/a", "/b"); // '\\a\\b'
```

```js
path.join("/a", "./b", "./c", "../d"); // '\\a\\b\\d'
```

## 参考链接：

### http://nodejs.cn/api/path.html#path_path_join_paths

### http://nodejs.cn/api/path.html#path_path_resolve_paths
