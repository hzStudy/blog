# npm-package 之 glob-all 使用

```
npm install --save glob-all
```

```
files
├── a.txt
├── b.txt
├── c.txt
└── x
    ├── y.txt
    └── z.txt
```

```js
var glob = require("glob-all");

var files = glob.sync([
  "files/**", //include all     files/
  "!files/x/**", //then, exclude   files/x/
  "files/x/z.txt" //then, reinclude files/x/z.txt
]);

console.log(files);

// [ 'files',
//   'files/a.txt',
//   'files/b.txt',
//   'files/c.txt',
//   'files/x/z.txt' ]
```

## 参考链接

### https://www.npmjs.com/package/glob-all
